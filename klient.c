#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <signal.h>
#include <ctype.h>
#include <getopt.h> 
#include <fcntl.h>



int data=0;

int sock = 0;

int confirmation=0;

int serverPid,signalNumber,chapter,interwal;

int acceptance=1, addresLong,offset,data;
int findChar(char c, char* buf);
char* rot13(char* string);
void sigHandler( int sigNum, siginfo_t *si, void *pVoid);
void decodingValue(int a);
void sigFunc();
char* itoa1(int val);
int codingData(int A, int B, int C, int D);


int main(int argc,char* argv[])
{

    int opt;


    char ch;

    char path[1024];

    int sflag=0,rflag=0,xflag=0,pflag=0,oflag=0,fflag=0;


    while ((opt = getopt (argc, argv, "s:r:x:p:o:f:")) != -1)
    {
        switch (opt)
        {
        case 's':
                    //printf ("Servers PID : \"%s\"\n", optarg);
                    sflag=1;
                    serverPid=strtol(optarg,NULL,0);
                    break;
        case 'r':
                    //printf ("RT Signal: \"%s\"\n", optarg);
                     rflag=1;
                     signalNumber=strtol(optarg,NULL,0);
                    break;
        case 'x':
                    //printf ("Numer ksiegi: \"%s\"\n", optarg);
                    xflag=1;
                    chapter=strtol(optarg,NULL,0);
                    break;
        case 'p':
                    //printf ("Sciezka do tablicy ogloszeniowej : \"%s\"\n", optarg);
                    pflag=1;
                    strcpy(path,optarg);
                    break;
        case 'o':
                    //printf ("Interwal : \"%s\"\n", optarg);
                    oflag=1;
                    interwal=strtol(optarg,NULL,0);
                    break;
         case 'f':
                    //printf ("Fragmentacja : \"%s\"\n", optarg);
                    fflag=1;
                    ch=optarg[0];

                    if((ch != 's') && (ch != 'l') && (ch != 'z'))
                    {
                        printf("Argument -f przyjmuje opcje [s,l,z]");
                        exit(0);
                    }
                    break;

        default:
            printf("Usage klient -s -o -x -f -r -p \n");
            exit(0);
        }
    }

    

    if(!sflag || !rflag || !oflag || !xflag || !pflag || !fflag )
    {
        printf("Usage klient -s -o -x -f -r -p \n");
        exit(0);
    }


    struct sigaction sa;
    memset( &sa, '\0', sizeof(sa));
    sa.sa_flags = SA_SIGINFO;
    sa.sa_sigaction = sigHandler;


    if( sigaction(SIGRTMIN+signalNumber, &sa, NULL) == -1 )
    {
        perror("Failed to handle signal!");
        exit(-1);
    }

    
    int s_client = socket(AF_UNIX, SOCK_DGRAM, 0);
    if (s_client < 0) 
    {
        perror("opening datagram socket");
        exit(1);
    }

    
    struct sockaddr_un server_address;
    memset(&server_address, 0, sizeof( struct sockaddr_un));
    server_address.sun_family = AF_UNIX;
    

    struct sockaddr_un client_address;
    memset(&client_address, 0, sizeof( struct sockaddr_un));
    client_address.sun_family = AF_UNIX;


    union sigval val;
    val.sival_int=codingData(signalNumber,chapter,ch,interwal);

    sigqueue(serverPid, SIGRTMIN+11, val);


    int end = 1;
    while(end ==1)
    {

        if(confirmation)

        {
            decodingValue(data);
            end = 0;    
        }
    
       
    }

    char s_server_name[108];
    if(!acceptance)
    {

    int fd1 = open(path, O_RDONLY);

    pread(fd1, &s_server_name, addresLong, offset);

    }
    else
    {
        printf("Serwer nie zaakceptowal polaczenia!! ");
        exit(0);
    }

   
    strncpy(&server_address.sun_path[1], &s_server_name[1], sizeof(server_address.sun_path) - 1);
    printf("server_address.sun_path: %c\n", server_address.sun_path[1]);
    server_address.sun_path[0] = 0;

    char s_client_name[] = "clikasjdkasjkdjaskjdksajksdajksadj";
    strcpy(client_address.sun_path, s_client_name);
    //name.sun_path[13] = '\0';

    int pid = getpid();
    printf("pid: %d\n", pid);

    char* string_pid;
    string_pid = itoa1(pid); 


    remove(client_address.sun_path);



    int client_bind = bind(s_client, (struct sockaddr *) &client_address, sizeof(struct sockaddr_un));
    if (client_bind == -1) perror("Bind: ");

    if(sendto(s_client, string_pid, sizeof(string_pid), 0, (struct sockaddr *) &server_address, sizeof(server_address)) < 0)
    {
        perror("Failed to send!");
    }

    int odczyt;
    int respons;

   
    char buf_socket[1024];
    
    while(1)
    {
        memset(&buf_socket,0,sizeof(buf_socket));
        odczyt = recvfrom(s_client, buf_socket, 1024, 0, NULL, NULL);
        if(odczyt == -1)
        {
            perror(" Failed to receve message!");
        }

        if( strcmp(buf_socket,"@Yeti") ==0 || strcmp(buf_socket,"@") == 0) break; // warunek konca transmisji
         
        //printf("Przeczytano: %d\n", odczyt);
        //printf("Bufor: %s\n", buf_socket);

         
        char* message = rot13(buf_socket);
        
        if((respons = sendto(s_client, message, 1024, 0, (struct sockaddr *) &server_address, sizeof(server_address))) < 0)
        {
            perror("Failed to send message!");
        }

    }



    unlink(client_address.sun_path);
    close(sock);
    return 0;
}

int findChar(char c, char* buf)
{
    int len = strlen(buf);

    for(int i = 0 ; i <= len; i++)
    {
        if( buf[i] == c)
            return i;
    }

    return -1;

}

char* rot13(char* string)
{
    int len = strlen(string);
    char* buffor = (char*)malloc(len+1);
    char charset[50] = "abcdefghijklmnoprstuwyz";
    char charset2[50] = "klmnoprstuwyzabcdefghij";

    char charset3[50] = "ABCDEFGHIJKLMNOPRTSUWYZ";
    char charset4[50] = "KLMNOPRSTUWYZABCDEFGHIJ";

    for(int i = 0; i<=len; i++)
    {
        if((string[i] < 64 || string[i] > 90) ) 
        {
            if((string[i] < 97 || string[i] > 123))
            { 
                buffor[i]=string[i]; 
                continue;
            }

            int d =findChar(string[i],charset);
            buffor[i] = charset2[d];
        
        }
        else if((string[i] < 97 || string[i] > 123) ) 
        {
            if((string[i] < 64 || string[i] > 90))
            { 
                buffor[i]=string[i]; 
                continue;
            }

            int d =findChar(string[i],charset3);
            buffor[i] = charset4[d];
        
        }

    }


    return buffor;
}

void sigHandler( int sigNum, siginfo_t *si, void *pVoid)
{
    if(sigNum == SIGRTMIN+signalNumber)
    {

        data=si->si_value.sival_int;
        confirmation=1;
    }

}

void decodingValue(int a)
{
        acceptance=  ((a & 0x000000FF)>>0);
        addresLong=  ((a & 0x0000FF00)>>8);
        offset = ((a & 0xFFFF0000)>>16);
}

void sigFunc()
{
    
}

char* itoa1(int val)
{ 
    static char buf[32] = {0}; 
   
    sprintf(buf,"%d",val); 

    return buf;
}

int codingData(int A, int B, int C, int D)
{
    data = (D<<24|C<<16|B<<8|A<<0);

    return data;
}
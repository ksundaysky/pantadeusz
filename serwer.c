#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <signal.h>
#include <ctype.h>
#include <getopt.h>
#include <fcntl.h>



char* b;
int a = 0;


int flaga=0;
int A,B,C,D;
int bajt1,bajt2,bajt3;
pid_t klientPID=0;
char panTadeusz[1024];

int readFromFile(int fd, int mode, char* buf);
int findChar(char c, char* buf);
char* rot13(char* string);
int setMode(int C);
char* setPath(int chapter);
int loadFile(int chapter,int mode);
struct timespec intervalTime(int interwal);
void signalHandeling( int sigNum, siginfo_t *si, void *pVoid);
void decodingValue(int a);
int codingData(int bajt1, int bajt2, int bajt3);
char* integerToCharArray(int val);


int main(int argc, char* argv[])
{
    
    int opt;

    char tablica[1024];

    while ((opt = getopt (argc, argv, "k:p:")) != -1)
     {
         switch(opt){

         
        case 'k':
                    strcpy(panTadeusz,optarg);
                    break;
        case 'p':
                    strcpy(tablica,optarg);
                    break;
        default:
            printf("Usage serwer -k -p \n");
            exit(0);

         }
     }
    

    pid_t PID = getpid();

    printf("PID : %d\n",PID);


    struct sigaction sa;
    memset( &sa, '\0', sizeof(sa));
    sa.sa_flags = SA_SIGINFO;
    sa.sa_sigaction = signalHandeling;

    if( sigaction(SIGRTMIN+11, &sa, NULL) == -1 )
    {
        perror("Failed to handle signal! ");
        exit(-1);
    }


    while(1)
    {
        if(flaga){
            decodingValue(a);
            break;

        }
    }
    
    if(flaga){

        int sock = socket(AF_UNIX, SOCK_DGRAM, 0);
        if (sock < 0) {
            perror("opening datagram socket");
            exit(1);
        }

        struct sockaddr_un s_server_address;

        s_server_address.sun_family=AF_UNIX;

        char* addr = integerToCharArray(klientPID);

        strncpy(s_server_address.sun_path,addr,sizeof(s_server_address.sun_path));

        s_server_address.sun_path[0]='\0';

        int err = bind(sock, (struct sockaddr*)&s_server_address, sizeof(s_server_address));
        if (err == -1) perror("Failed to bind!");

        int fd = open(tablica,O_CREAT| O_WRONLY | O_APPEND,0777);

        write(fd,s_server_address.sun_path,sizeof(s_server_address.sun_path));

        bajt1=0;
        bajt2=108;
        bajt3=lseek(fd,0,2)-108;

        close(fd);
        
        
        union sigval val;
        val.sival_int=codingData(bajt1,bajt2,bajt3);

        sigqueue(klientPID,SIGRTMIN+A, val);


        struct sockaddr_un client_address;
        socklen_t  client_address_len= sizeof(client_address); // do usuniecia


        memset(&client_address, 0, sizeof( struct sockaddr_un));

        
        char readingBuffer[1024];
        char readingBuffer2[1024];

        

        client_address.sun_family=AF_UNIX;
        
        err = recvfrom(sock, readingBuffer, 1024, 0, (struct sockaddr*)&client_address, &client_address_len);
        if (err == -1) perror("Failed to read!");
        

        int f = loadFile(B,1);

        clockid_t clockid;
        struct sigevent sevp;
        timer_t timerid;

        int tim = timer_create(clockid,&sevp,&timerid);

        
       while(1)
       {
        

        memset(&readingBuffer,0,sizeof(readingBuffer));
        memset(&readingBuffer2,0,sizeof(readingBuffer2));

        readFromFile(f,setMode(C),readingBuffer);


        int s = sendto(sock,readingBuffer,sizeof(readingBuffer),0,(struct sockaddr *)&client_address,client_address_len);
        if(s ==-1)
        {
            perror("Failed to send!");

        }
            if( strcmp(readingBuffer,"@Yeti") ==0 || strcmp(readingBuffer,"@") == 0) break;

        char*  check = rot13(readingBuffer);

        int respons = recvfrom(sock, readingBuffer2, 1024, 0, (struct sockaddr *)&client_address,&client_address_len);
            if(respons ==-1)
            {
                 perror("Faild to read!");
            }

                


                if(strcmp(check,readingBuffer2)==0)
                {
                   // printf("wszystko super mistrzu!");
                }
                else
                {
                   // printf("coś poszlo zle z tym rot13! :(");
                    exit(0);

                }
        }


        unlink(s_server_address.sun_path);
       

        return 1;

        }

}

int readFromFile(int fd, int mode, char* buf)
 {
    char c;
    int i = 0;
    if(mode == 2)
    {
        while(1)
        {
        read(fd, &c, 1 );
        if(c == ' ') return 1;
        else if( c == '\n') return 1;
        else 
        {
            buf[i]=c;
        }
        
        i++;
        }

    }
    else if(mode == 3)
    {
        
        read(fd, &c, 1 );
        
            buf[i]=c;
            return 2;
       

    }
    else
    {
        while(1)
        {
        read(fd, &c, 1 );
        if(c == '\n') return 1;
        else 
        {
            buf[i]=c;
        }
        
        i++;
        }

        return 1;

    }
    

     return 1;
 }
 int findChar(char c, char* buf)
{
    int len = strlen(buf);

    for(int i = 0 ; i <= len; i++)
    {
        if( buf[i] == c)
            return i;
    }

    return -1;

}

char* rot13(char* string)
{
    int len = strlen(string);
    char* buffor = (char*)malloc(len+1);
    char charset[50] = "abcdefghijklmnoprstuwyz";
    char charset2[50] = "klmnoprstuwyzabcdefghij";

    char charset3[50] = "ABCDEFGHIJKLMNOPRTSUWYZ";
    char charset4[50] = "KLMNOPRSTUWYZABCDEFGHIJ";

    for(int i = 0; i<=len; i++)
    {
        if((string[i] < 64 || string[i] > 90) ) 
        {
            if((string[i] < 97 || string[i] > 123))
            { 
                buffor[i]=string[i]; 
                continue;
            }

            int d =findChar(string[i],charset);
            buffor[i] = charset2[d];
        
        }
        else if((string[i] < 97 || string[i] > 123) ) 
        {
            if((string[i] < 64 || string[i] > 90))
            { 
                buffor[i]=string[i]; 
                continue;
            }

            int d =findChar(string[i],charset3);
            buffor[i] = charset4[d];
        
        }

    }

    return buffor;
}

int setMode(int C)
{
    if(C == 'l') return 1;
    else if( C == 's') return 2;
    else if( C == 'z') return 3;
    else 
    {
        printf("Zla literka :( ");
        return -1;
    } 
}

char* setPath(int chapter)
 {

    char* pathName = (char*)malloc(128);

    if(strlen(panTadeusz)!=0){
    strcpy(pathName,panTadeusz);
    char slash[2] = "/";
    strcat(pathName,slash);
    }


    switch(chapter)
    {
        case 1:
            strcat(pathName,"ksiega1.txt");
            break;
        case 2:
            strcat(pathName,"ksiega2.txt");
            break;
        case 3:
            strcat(pathName,"ksiega3.txt");
            break;
        case 4:
            strcat(pathName,"ksiega4.txt");
            break;
        case 5:
            strcat(pathName,"ksiega5.txt");
            break;
        case 6:
            strcat(pathName,"ksiega6.txt");
            break;
        case 7:
            strcat(pathName,"ksiega7.txt");
            break;
        case 8:
            strcat(pathName,"ksiega8.txt");
            break;
        case 9:
            strcat(pathName,"ksiega9.txt");
            break;
        case 10:
            strcat(pathName,"ksiega10.txt");
            break;
        case 11:
            strcat(pathName,"ksiega11.txt");
            break;
        case 12:
            strcat(pathName,"ksiega12.txt");
            break;
        default:
            printf("bledna ksiega");
            exit(0);
    }

    
   

    return pathName;

 }

int loadFile(int chapter,int mode)
{
    char* path = setPath(chapter);

    int fd = open(path,O_RDONLY);

    return fd;
}

struct timespec intervalTime(int interwal)
{
    struct timespec t1;
    double value = (double)interwal/64;



    t1.tv_sec=(int)value;
    double val=value-(int)value;
    int val2 = val*1000000000;
    printf("val2 : %d", val2);
    t1.tv_nsec=val2;


    return t1;
}

void signalHandeling( int sigNum, siginfo_t *si, void *pVoid)
{
    if(sigNum == SIGRTMIN+11)
    {
        flaga=1;
        a = si->si_value.sival_int;
        klientPID=si->si_pid;
    }

}

void decodingValue(int a)
{
    A=  ((a & 0x000000FF)>>0);
    B=  ((a & 0x0000FF00)>>8);
    C = ((a & 0x00FF0000)>>16);
    D = ((a & 0xFF000000)>>24);
}

int codingData(int bajt1, int bajt2, int bajt3)
{
    int data = (bajt3<<16|bajt2<<8|bajt1<<0);

    return data;
}

char* integerToCharArray(int val)
{ 
    static char buf[32] = {0}; 
   
    sprintf(buf,"%d",val); 

    return buf;
}